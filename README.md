# PaintApp
A java paint application

```
The program layout consist mainly of a 2D drawing space along with the ability to select basic modes as follows:
- A mode to draw sketches using scribbled freehand lines
- A mode to draw straight lines
- A mode to draw rectangles
- A mode to draw ellipses
- A mode to draw special cases of these (squares and circles)
- A mode to draw polygons (open and closed polygons)
In addition, the user should be able to select a colour for any of the graphical objects that are about to be drawn. Furthermore, the user should have a selection method so that they can identify an object that has already been drawn and perform the following operations:
- Move the selected object to a new location
- Cut the object from the drawing (delete a graphical object)
- Paste the selected object (copy and paste) to a new location
Advanced versions of the program may also have the following functionality:
- Group the object with another object (possibly creating groups of arbitrary objects)
- Ungroup a set of objects that have been grouped
Very advanced versions of the program could contain the following additional functions:
- Undo (and Redo)
- Save (and Load) a partially completed drawing
```
##Collaborators:
```
tarandeepkaursahney (https://github.com/tarandeepkaursahney)
Samira-e (https://github.com/Samira-e)
ajai-maxwel(https://bitbucket.org/ajai_maxwel/)
nvjsingh (https://github.com/nvjsingh)
```
## Prerequisites:

* Java version "1.8.0_131" must be installed on your system to run the application
* JDK version "1.8.0_131" must be installed on your system if you want to compile and create executable

## Running executable
Double click on PaintApp.jar

## Instructions to compile and run PaintApp from command line

Run the following commands in "PaintApp" directory:

```
javac -cp src\Paint -d classes src\Paint\*.java
java -cp classes Paint.SketchApp
```

## Creating the executable
```
javac -cp src\Paint -d classes src\Paint\*.java
jar cfm PaintApp.jar Manifest.txt -C classes .
```